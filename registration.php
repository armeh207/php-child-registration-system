<?php
session_start();
require('header.php');
error_reporting(0);
@require "../../mysql_connect.php";
 ?>
 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title>Registration</title>
   </head>
   <body style="background-image: url('photos/navigation.png'); display= grid;">
     <div class="container" style="background-color: white; position: sticky; top: 150px; margin-left: auto; margin-right: auto;width: 600px;">
       <div style='margin-left: 20px;'>
     <?php
     if(!isset($_SESSION['user_id'])){
       echo "<br>
             <p>you have to register first.</p>
             <form action='register_user.php' method='post'>
             First name:<input type='text' name='firstName' maxlength='100' required><br>
             Last name:<input type='text' name='lastName' maxlength='100' required><br>
             Email:<input type='email' name='email'maxlength='320' required><br>
             Phone number: <input type='tel' name='phoneNo' maxlength='35' required><br>
             Username: <input type='text' name='username' maxlength='400' required><br>
             Password: <input type='password' name='password' maxlength='50' required><br>
             <input type='submit' value='Register'>
             </form>
             </div>
             <br>";
     }
     else {
       $query = "SELECT * FROM fees";
       $result = mysqli_query($db_connection, $query);
       if($result){
         $row = mysqli_fetch_array($result);
         echo "<br><div style='margin-left: 20px;'><form action = register_child.php method='post'>
            First name: <input type='text' name='firstName' maxlength='100' required><br>
            Last name: <input type='text' name='lastName' maxlength='100' required><br>
            <input type='radio' name='type' value='baby' required> Babies
       			<select name='babyAge'>
       			<option value='6 months'>6 months old</option>
       			<option value='7 months'>7 months old</option>
       			<option value='8 months'>8 months old</option>
       			<option value='9 months'>9 months old</option>
       			<option value='10 months'>10 months old</option>
       			<option value='11 months'>11 months old</option>
       			<option value='12 months'>12 months old</option>
       			</select>
            <input type='number' name='babyFee' readonly value='{$row['babyFee']}'><br>
       			<input type='radio' name='type' value='wobbler' /> Wobbler
       			<select name='wobblerAge' >
       			<option value='13 months'>13 months old</option>
       			<option value='14 months'>14 months old</option>
       			<option value='15 months'>15 months old</option>
       			<option value='16 months'>16 months old</option>
       			<option value='17 months'>17 months old</option>
       			<option value='18 months'>18 months old</option>
       			<option value='19 months'>19 months old</option>
       			<option value='20 months'>20 months old</option>
       			<option value='21 months'>21 months old</option>
       			<option value='22 months'>22 months old</option>
       			<option value='23 months'>23 months old</option>
       			</select>
            <input type='number' name='wobblerFee' readonly value='{$row['wobblerFee']}'><br>
       			<input type='radio' name='type' value='toddler' /> Toddlers
       			<select name='toddlerAge' >
       			<option value='2 years'>2 years old</option>
       			<option value='3 years'>3 years old</option>
       			</select>
            <input type='number' name='toddlerFee' readonly value='{$row['toddlerFee']}'><br>
       			<input type='radio' name='type' value='preschool' /> Preschool
       			<select name='preschoolAge' >
       			<option value='3 years'>4 years old</option>
       			<option value='5 years'>5 years old</option>
       			<option value='6 years'>6 years old</option>
       			</select>
            <input type='number' name='preschoolFee' readonly value='{$row['preschoolFee']}'>
       			<br>
       			<input type='submit' value='Register'>
                  </form><br>";
          }
     }
     echo "<p class = 'editLink'><a href='registration_edit.php'>Edit fees</a></p><br>";
     
      ?>
    </div>
    </div>
   </body>
 </html>
