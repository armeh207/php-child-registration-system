<?php
session_start();
@require "../../mysql_connect.php";
error_reporting(0);
$firstName = pass_input($_POST['name']);
$service = pass_input($_POST['service']);
$date = pass_input($_POST['date']);
$comment = pass_input($_POST['comment']);
$errors = array();
if(empty($firstName)){
  $errors[] = "You forgot to enter first name.";
}
else {
  $firstName = mysqli_real_escape_string($db_connection, $firstName);
}

if(empty($service)){
  $errors[] = "You forgot to enter service.";
}
else {
  $service = mysqli_real_escape_string($db_connection, $service);
}

if(empty($date)){
  $errors[] = "You forgot to enter Date.";
}
else {
  $date = mysqli_real_escape_string($db_connection, $date);
}

if(empty($comment)){
  $errors[] = "You forgot to enter comment.";
}
else {
  $comment = mysqli_real_escape_string($db_connection, $comment);
}


if(empty($errors)){
 $query = "INSERT INTO testimonial (`firstName`, `service`, `date`, `comment`) VALUES('{$firstName}', '{$service}', '{$date}', '{$comment}');";
  $result = mysqli_query($db_connection, $query);
  if($result){
    header('Refresh: 3; URL=index.php');
    echo "Testimonial successfully made. Redirecting to home page. ";
  }
}
else {
  header('Refresh: 3; URL=index.php');
  echo "The following errors occured:<br> ";
  foreach ($errors as $key => $value) {
    echo "$value <br>";
  }
}
function pass_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = strip_tags($data);
  return $data;
}
?>
