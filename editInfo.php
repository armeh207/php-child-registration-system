<?php
session_start();
@require "../../mysql_connect.php";
error_reporting(0);
$updatedBox1 = pass_input($_POST['updatedBox1']);
$updatedBox2 = pass_input($_POST['updatedBox2']);
$errors = array();
if(!empty($updatedBox1)){
  $updatedBox1 = mysqli_real_escape_string($db_connection,$updatedBox1);
}
else {
  $errors[] = "Box 1 cannot be empty";
}
if(!empty($updatedBox2)){
  $updatedBox2 = mysqli_real_escape_string($db_connection ,$updatedBox2);
}
else {
  $errors[] = "Box 2 cannot be empty";
}
if(empty($errors)){
  $query = "UPDATE page SET box1='{$updatedBox1}', box2='{$updatedBox2}'";
  $result = mysqli_query($db_connection, $query);
  if($result){
    header('Refresh: 3; URL=index.php');
    echo "Page updated successfully. Redirecting to home page.";
  }
}
else {
	header('Refresh: 3; URL=edit.php');
  echo "Following errors occured: ";
  foreach ($errors as $key => $value) {
    echo "$value <br>";
  }
  echo "Redirecting to edit page and enter details again";
}
function pass_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = strip_tags($data);
  return $data;
}
?>
