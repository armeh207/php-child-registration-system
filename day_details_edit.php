<?php
session_start();
require('header.php');
@require "../../mysql_connect.php";
error_reporting(0);
if($_SERVER['REQUEST_METHOD'] == "POST"){
	if(isset($_POST['add'])){
		echo "
		<body style='background-image: url('photos/navigation.png'); display= grid; text-align:center;'>
		    <div class='container' style='background-color: white; position: sticky; top: 150px; margin-left: auto; margin-right: auto;width: 600px;'>
		<h2>Adding Day Details</h2>
		<form action='day_details_edit.php' method='post'>
			<br>
             Child's FullName: <input type='text' name='name' maxlength='100' required><br><br>
             Temperature(°C): <input type='number' name='temp' maxlength='100' required><br><br>
			 Date: <input  type='date'  name='date' /><br><br>
             Breakfast: <input type='text' name='breakfast' maxlength='100' required><br><br>
             Lunch: <input type='text' name='lunch' maxlength='100' required><br><br>
			 Activities: <br><textarea name='activity' type='text'rows='10' cols='50' onkeyup='Allow()' required></textarea><br><br>
			<input type='submit' value='Submit'></div></body>";
	}
	else{
	$name=pass_input($_POST['name']);
	$temp=$_POST['temp'];
	$date=$_POST['date'];
	$breakfast=pass_input($_POST['breakfast']);
	$lunch=pass_input($_POST['lunch']);
	$activity=pass_input($_POST['activity']);
	$errors = array();

if (empty($name)) {
 $errors[] = 'You forgot to enter user name';
}
else {
  $name = mysqli_real_escape_string($db_connection, $name);
}

if (empty($temp)) {
  $errors[] = 'You forgot to enter Temperature';
} else {
  $temp = mysqli_real_escape_string($db_connection, $temp);
}

if (empty($date)) {
  $errors[] = 'You forgot to enter Date';
} else {
  $date = mysqli_real_escape_string($db_connection, $date);
}

if (empty($breakfast)) {
  $errors[] = 'You forgot to enter Breakfast';
} else {
  $breakfast = mysqli_real_escape_string($db_connection, $breakfast);
}

if (empty($lunch)) {
  $errors[] = 'You forgot to enter Lunch';
} else {
  $lunch = mysqli_real_escape_string($db_connection, $lunch);
}

if (empty($activity)) {
  $errors[] = 'You forgot to enter Activities';
} else {
  $activity = mysqli_real_escape_string($db_connection, $activity);
}

if(empty($errors)){
  $query = "INSERT INTO `day`(`childName`, `temperature`, `date`, `breakfast`, `lunch`, `activities`) VALUES ('{$name}','{$temp}','{$date}','{$breakfast}','{$lunch}','{$activity}')";
  $result = mysqli_query($db_connection, $query);

  if($result){
	header('Refresh: 3; URL=day_details_options.php');
    echo "Succesfully Added. Redirecting back to day detail options...";
}
}
	}
}

if($_SERVER['REQUEST_METHOD'] == "GET"){
	if(isset($_GET['edit'])){
	echo "
	
		    <div class='container' style='background-color: white; position: sticky; top: 150px; margin-left: auto; margin-right: auto;width: 600px;'>
	<h2>Editing Day Details</h2>
	<form action='day_details_edit.php' method='get'>
	<br>
	Child's FullName (This is what will be used to find what is being edited) : <input type='text' name='name' maxlength='100' required><br><br>
	Temperature(°C): <input type='number' name='temp' maxlength='100' required><br><br>
	Date: <input  type='date'  name='date' /><br><br>
    Breakfast: <input type='text' name='breakfast' maxlength='100' required><br><br>
    Lunch: <input type='text' name='lunch' maxlength='100' required><br><br>
	Activities: <br><textarea name='activity' type='text'rows='10' cols='50' onkeyup='Allow()' required></textarea><br><br>
	</form></div>";
	}
	else{
	$name=pass_input($_GET['name']);
	$temp=$_GET['temp'];
	$date=$_GET['date'];
	$breakfast=pass_input($_GET['breakfast']);
	$lunch=pass_input($_GET['lunch']);
	$activity=pass_input($_GET['activity']);
	$errors = array();

	if (empty($name)) {
 $errors[] = 'You forgot to enter Childs Fullname';
}
else {
  $name = mysqli_real_escape_string($db_connection, $name);
}

if (empty($temp)) {
  $errors[] = 'You forgot to enter Temperature';
} else {
  $temp = mysqli_real_escape_string($db_connection, $temp);
}

if (empty($date)) {
  $errors[] = 'You forgot to enter Date';
} else {
  $date = mysqli_real_escape_string($db_connection, $date);
}

if (empty($breakfast)) {
  $errors[] = 'You forgot to enter Breakfast';
} else {
  $breakfast = mysqli_real_escape_string($db_connection, $breakfast);
}

if (empty($lunch)) {
  $errors[] = 'You forgot to enter Lunch';
} else {
  $lunch = mysqli_real_escape_string($db_connection, $lunch);
}

if (empty($activity)) {
  $errors[] = 'You forgot to enter Activities';
} else {
  $activity = mysqli_real_escape_string($db_connection, $activity);
}

if(empty($errors)){
	$query = "UPDATE day SET temperature='{$temp}',date='{$date}',breakfast='{$breakfast}',lunch='{$lunch}',activities='{$activities}' WHERE childName='{$name}'";
    $result = mysqli_query($db_connection, $query);

	if($result){
	header('Refresh: 3; URL=day_details_options.php');
    echo "Succesfully Edited. Redirecting back to day detail options...";
}
}
else{
	header('Refresh: 3; URL=index.php');
  echo "The following errors occured:<br> ";
  foreach ($errors as $key => $value) {
    echo "$value <br>";
  }
}
	}
}
    
function pass_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = strip_tags($data);
  return $data;
}
?>
