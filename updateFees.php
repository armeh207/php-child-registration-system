<?php
@session_start();
require('header.php');
@require "../../mysql_connect.php";
error_reporting(0);

$updatedBabyFee = pass_input($_POST['updatedBabyFee']);
$updatedWobblerFee = pass_input($_POST['updatedWobblerFee']);
$updatedToddlerFee = pass_input($_POST['updatedToddlerFee']);
$updatedPreschoolFee = pass_input($_POST['updatedPreschoolFee']);
$errors = array();

if(empty($updatedBabyFee)){
  $errors[] = "you forgot to enter updated baby fee.";
}
else {
  $updatedBabyFee = mysqli_real_escape_string($db_connection, $updatedBabyFee);
}
if(empty($updatedWobblerFee)){
  $errors[] = "you forgot to enter updated wobbler fee.";
}
else {
  $updatedWobblerFee = mysqli_real_escape_string($db_connection, $updatedWobblerFee);
}
if(empty($updatedToddlerFee)){
  $errors[] = "you forgot to enter updated toddler fee.";
}
else {
  $updatedToddlerFee = mysqli_real_escape_string($db_connection, $updatedToddlerFee);
}
if(empty($updatedPreschoolFee)){
  $errors[] = "you forgot to enter updated preschool fee.";
}
else {
  $updatedPreschoolFee = mysqli_real_escape_string($db_connection, $updatedPreschoolFee);
}

if(empty($errors)){
$query = "UPDATE fees SET babyFee='{$updatedBabyFee}', wobblerFee='{$updatedWobblerFee}', toddlerFee='{$updatedToddlerFee}', preschoolFee='{$updatedPreschoolFee}'";
$result = mysqli_query($db_connection, $query);
if($result){
  header('Refresh: 3; URL=index.php');
  echo "Fees info updated successfully. Redirecting to home page";
}
else{
  header('Refresh: 3; URL=index.php');
  echo "Fees info updating was not successful. Redirecting to home page";
}
}
else{
	header('Refresh: 3; URL=registration.php');
  echo "Following errors occured: ";
  foreach ($errors as $key => $value) {
    echo "$value <br>";
  }
  echo "Redirecting to Registration...";
}
function pass_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = strip_tags($data);
  return $data;
}
 ?>
