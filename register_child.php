<?php
session_start();
require('header.php');
error_reporting(0);
@require "../../mysql_connect.php";
$firstName = pass_input($_POST['firstName']);
$lastName = pass_input($_POST['lastName']);
$type = pass_input($_POST['type']);
$age = pass_input($_POST["$type"."Age"]);
$parentName = $_SESSION['name'];
$fee = pass_input($_POST["$type"."Fee"]);
$errors = array();
if(empty($firstName)){
  $errors = "You forgot to enter first name.";
}
else {
  $firstName = mysqli_real_escape_string($db_connection, $firstName);
}
if(empty($lastName)){
  $errors = "You forgot to enter last name.";
}
else {
  $lastName = mysqli_real_escape_string($db_connection, $lastName);
}
if(empty($type)){
  $errors = "You forgot to select type.";
}
else {
  $type = mysqli_real_escape_string($db_connection, $type);
}
if(empty($age)){
  $errors = "You forgot to select age.";
}
else {
  $age = mysqli_real_escape_string($db_connection, $age);
}
if(empty($errors)){
  $query = "INSERT INTO child VALUES('{$firstName}', '{$lastName}', '{$type}', '{$age}', '{$parentName}', '{$fee}')";
  $result = mysqli_query($db_connection, $query);
  if($result){
    header('Refresh: 3; URL=index.php');
    echo "Child registered successfully. Redirecting to home page";
  }
  else {
    header('Refresh: 3; URL=index.php');
    echo "Something went wrong. Please try again later";
  }
}
else{
	header('Refresh: 3; URL=index.php');
  echo "The following errors occured:<br> ";
  foreach ($errors as $key => $value) {
    echo "$value <br>";
  }
}
function pass_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = strip_tags($data);
  return $data;
}
 ?>
