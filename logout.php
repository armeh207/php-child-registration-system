<?php
error_reporting(0);
session_start();
header('Location: index.php');
if (isset($_SESSION['user_id'])) {
  session_unset();
  session_destroy();
  echo "logged out. <a href='index.php'>Home page</a>";
}else {
  echo "Already logged out!";
}
 ?>
