<?php
session_start();
error_reporting(0);
$user = pass_input($_POST['username']);
$pass = pass_input($_POST['password']);
@require "../../mysql_connect.php";
$errors = array();
if (empty($user)) {
  $errors[] = 'You forgot to enter user name';
} else {
  $user = mysqli_real_escape_string($db_connection, $user);
}
if (empty($pass)) {
  $errors[] = 'You forgot to enter password';
} else {
  $pass = mysqli_real_escape_string($db_connection, $pass);
}
if(empty($errors)){
  $query = "SELECT * FROM user WHERE username='$user'";
  $result = @mysqli_query($db_connection, $query);
  $row = @mysqli_fetch_array($result);
if(mysqli_num_rows($result) == 1 && password_verify($pass, $row['password'])) {
  @$_SESSION['user_id']  = $row['username'];
  $_SESSION['name'] = $row['firstName']." ".$row['lastName'];
  header('Refresh: 3; URL=index.php');
  echo "<h1>Logged In!</h1> <p>Welcome {$_SESSION['name']}. You will be redirected to home page in 3 seconds. </p>";
} else {
  header('Refresh: 3; URL=index.php');
  echo "<h2>Error!</h2> <h3>The username and password are incorrect! Redirecting to home page in 3 seconds. </h3>";
}
}
else{
  echo "The following errors occured:<br> ";
  foreach ($errors as $key => $value) {
    echo "$value <br>";
  }
  echo "Please <a href='index.php'>go back</a> and enter details!";
}

function pass_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = strip_tags($data);
  return $data;
}
?>
