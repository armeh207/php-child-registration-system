<?php
session_start();
require('header.php');
@require "../../mysql_connect.php";

if($_SERVER['REQUEST_METHOD'] == "POST"){
$firstName = pass_input($_POST['firstName']);
$lastName = pass_input($_POST['lastName']);
$email = pass_input($_POST['email']);
$phoneNo = pass_input($_POST['phoneNo']);
$username = pass_input($_POST['username']);
$password = pass_input($_POST['password']);

$errors = array();
if(empty($firstName)){
  $errors[] = "You forgot to enter first name.";
}
else {
  $firstName = mysqli_real_escape_string($db_connection, $firstName);
}
if(empty($lastName)){
  $errors[] = "You forgot to enter last name.";
}
else {
  $lastName = mysqli_real_escape_string($db_connection, $lastName);
}
if(empty($email)){
  $errors[] = "You forgot to enter email.";
}
else {
  $email = mysqli_real_escape_string($db_connection, $email);
}
if(empty($phoneNo)){
  $errors[] = "You forgot to enter phone number.";
}
else {
  $phoneNo = mysqli_real_escape_string($db_connection, $phoneNo);
}
if(empty($username)){
  $errors[] = "You forgot to enter username.";
}
else {
  $username = mysqli_real_escape_string($db_connection, $username);
}
if(empty($password)){
  $errors[] = "You forgot to enter password.";
}
else {
  $password = mysqli_real_escape_string($db_connection, $password);
}
//Encrypt password using password_hash:
$encryptedPass = password_hash($password, PASSWORD_DEFAULT);
$pattern = '/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/';//pattern for strong password
if(checkIfUsernameExists($db_connection, $username)){
  $errors[] = "The username you entered already exists. Enter a new username.<br>";
}
if(!preg_match($pattern, $password)){
  $errors[] = "The password you entered is not strong enough. It must be at least 8 characters long, include at least 1 uppercase letter, 1 lowercase letter, 1 digit and 1 special character";
}

if(empty($errors)){
  $query = "INSERT INTO user VALUES('{$firstName}', '{$lastName}', '{$email}', '{$phoneNo}', '{$username}', '{$encryptedPass}')";
  $result = mysqli_query($db_connection, $query);
  
$headers = 'From: childcare@example.com' . "\r\n" . 'Reply-To: childcare@example.com' .
"\r\n". 'X-Mailer: PHP/' . phpversion();
$subject="Succesfully Registered";
$message="This is to notify you that you have successfully been registered";

  if($result){
	  $sent=mail($email, $subject, $message,$headers);
	  if($sent){
		echo "Email sent<br>";  
	  }
	  else{
		  echo "Email not sent<br>";
	  }
    header('Refresh: 3; URL=index.php');
    echo "Registered successfully. Redirecting to home page and you will be able to log in now.";
  }

}
else {
  echo "The following errors occured:<br> ";
  foreach ($errors as $key => $value) {
    echo "$value <br>";
  }
  echo "Enter details again<br>";
  echo "<form action='register_user.php' method='post'>
    First name:<input type='text' name='firstName' maxlength='100' value='{$firstName}' required><br>
    Last name:<input type='text' name='lastName' maxlength='100' value='{$lastName}' required><br>
    Email:<input type='email' name='email'maxlength='320' value='{$email}' required><br>
    Phone number: <input type='tel' name='phoneNo' maxlength='35' value='{$phoneNo}' required><br>
    Username: <input type='text' name='username' maxlength='400' required><br>
    Password: <input type='password' name='password' maxlength ='50' required><br>
	
	
    <input type='submit' value='Register'>
  </form>";
}
}

if($_SERVER['REQUEST_METHOD'] == "GET"){
	$childfirstName = pass_input($_GET['childfirstName']);
	$childlastName = pass_input($_GET['childlastName']);
	$childtype = $_GET['type'];
	$age="";
	$errors2 = array();
	$parent=$_SESSION['name'];
	
if($childtype=="babies"){
$age = $_GET['babyage'];
}

if($childtype=="wobblers"){
$age = $_GET['wobblerage'];
}
if($childtype=="toddler"){
$age = $_GET['toddlerage'];
}

if($childtype=="preschool"){
$age = $_GET['preschoolage'];
}
	

if(empty($childtype)){
  $errors2[] = "You forgot to enter Child type.";
}
else {
  $childtype = mysqli_real_escape_string($db_connection, $childtype);
}

if(empty($errors2)){
	
	
  $query = "INSERT INTO child VALUES('{$childfirstName}', '{$childlastName}', '{$childtype}', '{$age}', '{$parent}')";
  $result = mysqli_query($db_connection, $query);
  
  if($result){
    header('Refresh: 3; URL=index.php');
    echo "Registered successfully. Redirecting to home page and you will be able to log in now.";
  }

}
else {
  echo "The following errors occured:<br> ";
  foreach ($errors2 as $key => $value) {
    echo "$value <br>";
  }
  echo "Enter details again<br>";
  echo "<form action='register_user.php' method='get'>
			Child's First name:<input type='text' name='childfirstName' maxlength='100' value='{$firstName}' required><br>
			Child's Last name:<input type='text' name='childlastName' maxlength='100' value='{$lastName}' required><br>
		   <input type='radio' name='type' value='babies' /> Babies 
			<select name='babyage' >
			<option value='6 months'>6 months old</option>
			<option value='7 months'>7 months old</option>
			<option value='8 months'>8 months old</option>
			<option value='9 months'>9 months old</option>
			<option value='10 months'>10 months old</option>
			<option value='11 months'>11 months old</option>
			<option value='12 months'>12 months old</option>
			</select>
			
			<input type='radio' name='type' value='wobblers' /> Wobblers 
			<select name='wobblerage' >
			<option value='13 months'>13 months old</option>
			<option value='14 months'>14 months old</option>
			<option value='15 months'>15 months old</option>
			<option value='16 months'>16 months old</option>
			<option value='17 months'>17 months old</option>
			<option value='18 months'>18 months old</option>
			<option value='19 months'>19 months old</option>
			<option value='20 months'>20 months old</option>
			<option value='21 months'>21 months old</option>
			<option value='22 months'>22 months old</option>
			<option value='23 months'>23 months old</option>
			</select>
			
			<input type='radio' name='type' value='toddlers' /> Toddlers
			<select name='toddlerage' >
			<option value='2 years'>2 years old</option>
			<option value='3 years'>3 years old</option>
			</select>
			
			<input type='radio' name='type' value='preschool' /> Preschool
			<select name='preschoolage' >
			<option value='3 years'>4 years old</option>
			<option value='5 years'>5 years old</option>
			<option value='6 years'>6 years old</option>
			</select>
			
			<input type='submit' value='Register'>
			</form>";
}

}
function checkIfUsernameExists($db_connection, $username) {
  $query = "SELECT username FROM user WHERE username='{$username}'";
  $result = mysqli_query($db_connection, $query);
  if(mysqli_num_rows($result)==0){
    return false;
  }
  else {
    return true;
  }
}
function pass_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = strip_tags($data);
  return $data;
}
 ?>
