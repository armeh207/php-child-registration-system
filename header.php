<?php
error_reporting(0);
@require "../../mysql_connect.php";
echo "<style type='text/css'>
/* Add a black background color to the top navigation */
.navigation{
  background:  #ffc4c4;
  overflow: hidden;
}

/* Style the links inside the navigation bar */
.navigation a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 18px;
  font-weight: bold;
  color: #4a6fa8;
}

.editLink{
  display: none;
}
.manageLink{
  display: none;
}

/* Change the color of links on hover */
.navigation a:hover {
  color: #3a61d6;
}
.userLink {
    display: none;
}

</style>";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
<div class="navImage">
<nav class="navigation">
  <ul class="list" style="list-style: none;">
    <li><a href="index.php">Home</a></li>
    <li class="regLink"><a href="registration.php">Registration</a></li>
    <li><a href="services.php">Services and facilities</a></li>
    <li class="userLink"><a href="day_details.php">Day details</a></li>
    <li><a href="testimonial.php">Testimonials</a></li>
    <li><a href="contact_us.php">Contact us</a></li>
    <li class="userLink"><a href="logout.php">Logout</a></li>
  </ul>
  <?php
  if (isset($_SESSION['user_id'])) {
    if($_SESSION['user_id']=="admin"){//if user is admin, enable edit link
      echo "<style type='text/css'>
            .editLink {
                display: block;
            }
            .manageLink{
              display:block;
            }
            </style>";
    }
    echo "<style type='text/css'>
          .userLink {
              display: block;
          }
          </style>";
		  
		  echo "<style type='text/css'>
            #loginForm{
                display: none;
            }
            </style>";
    }
  ?>
</nav>
</div>
  </body>
</html>
